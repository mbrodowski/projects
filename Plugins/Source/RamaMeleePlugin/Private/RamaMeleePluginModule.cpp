// Copyright 2015 by Nathan "Rama" Iyer. All Rights Reserved.
#include "RamaMeleePluginPrivatePCH.h"

DEFINE_LOG_CATEGORY(RamaMeleePlugin)

IMPLEMENT_MODULE(FDefaultGameModuleImpl, RamaMeleePlugin);